import { TennisAppPage } from './app.po';

describe('tennis-app App', function() {
  let page: TennisAppPage;

  beforeEach(() => {
    page = new TennisAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
