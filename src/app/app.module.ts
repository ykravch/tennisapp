import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AngularFireModule, AuthProviders, AuthMethods} from 'angularfire2';
import { TennisAppRoutingModule } from './app-routing.module';

import { MaterialModule } from '@angular/material';
import { MdToolbarModule } from '@angular/material/toolbar';
import { MdCardModule } from '@angular/material/card';
import { MdIconModule } from '@angular/material/icon';
import { MdButtonModule } from '@angular/material/button';
import { MdSidenavModule } from '@angular/material/sidenav';

import { AppComponent } from './app.component';
import { TeachersListComponent } from './teacher/teachers-list/teachers-list.component';
import { TeacherCardComponent } from './teacher/teacher-card/teacher-card.component';
import { OwnerPageComponent } from './owner-page/owner-page.component';
import { MenuComponent } from './menu/menu.component';
import { OwnerDashboardComponent } from './owner-dashboard/owner-dashboard.component';
import { TeachersPageComponent } from './teacher/teachers-page/teachers-page.component';
import { AddTeacherComponent } from './teacher/add-teacher/add-teacher.component';
import { GroupsListComponent } from './group/groups-list/groups-list.component';
import { GroupsPageComponent } from './group/groups-page/groups-page.component';
import { PageComponent } from './page/page.component';
import { StudentsPageComponent } from './students/students-page/students-page.component';
import { TeacherInfoComponent } from './teacher/teacher-info/teacher-info.component';
import { EmptyComponent } from './empty/empty.component';
import { AddGroupComponent } from './group/add-group/add-group.component';
import { StudentsListComponent } from './students/students-list/students-list.component';
import { GroupCardComponent } from './group/group-card/group-card.component';
import { GroupInfoComponent } from './group/group-info/group-info.component';
import { LoginComponent } from './auth/login/login.component';
import { ChatComponent } from './chat/chat/chat.component';
import { ChatMessagesListComponent } from './chat/chat-messages-list/chat-messages-list.component';
import { ChatAddMessageComponent } from './chat/chat-add-message/chat-add-message.component';
import { ChatMessageComponent } from './chat/chat-message/chat-message.component';
import {AuthService} from "./auth/auth.service";
import { LogoutComponent } from './auth/logout/logout.component';

const firebaseConfig = {
  apiKey: "AIzaSyABnx__0UIJTYmTFBuWg033aWZshLaKcsw",
  authDomain: "tennisapp-16425.firebaseapp.com",
  databaseURL: "https://tennisapp-16425.firebaseio.com",
  storageBucket: "tennisapp-16425.appspot.com",
  messagingSenderId: "464801243332"
};

@NgModule({
  declarations: [
    AppComponent,
    TeachersListComponent,
    TeacherCardComponent,
    OwnerPageComponent,
    MenuComponent,
    OwnerDashboardComponent,
    TeachersPageComponent,
    AddTeacherComponent,
    GroupsListComponent,
    GroupsPageComponent,
    PageComponent,
    StudentsPageComponent,
    TeacherInfoComponent,
    EmptyComponent,
    AddGroupComponent,
    StudentsListComponent,
    GroupCardComponent,
    GroupInfoComponent,
    LoginComponent,
    ChatComponent,
    ChatMessagesListComponent,
    ChatAddMessageComponent,
    ChatMessageComponent,
    LogoutComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TennisAppRoutingModule,
    MdToolbarModule.forRoot(),
    MdCardModule.forRoot(),
    MdIconModule.forRoot(),
    MdButtonModule.forRoot(),
    MdSidenavModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig, {
      provider: AuthProviders.Password,
      method: AuthMethods.Password
    }),
    MaterialModule.forRoot()
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
