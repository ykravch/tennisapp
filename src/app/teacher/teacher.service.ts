import { Injectable } from '@angular/core';
import {Teacher} from "./teacher";
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class TeacherService {
  context: any; // TODO: fix interface
  list: FirebaseListObservable<Teacher[]>;

  constructor(private af: AngularFire, private auth: AuthService) {
    this.context = this.auth.getContext();
    this.list = this.af.database.list(`${this.context.organization}/teachers`);
  }

  addTeacher(teacher: Teacher) {

    this.af.auth.createUser({email: 'yevhenii.kravchenko@gmail.com', password: 'test1234!'}).then(newUser => {
      // Save user to custom user table
      console.log("new user:", newUser);

      this.af.database.object(`${this.context.organization}/users/${(newUser as any).$key}`).set({
        name: teacher.name
      });

      this.list.push(teacher);

      this.af.database.object(`organizationByUser/${(newUser as any).$key}`).set(this.context.organization);

      this.auth.resetPassword('yevhenii.kravchenko@gmail.com');
    });
  };

  getTeachers(): FirebaseListObservable<Teacher[]> {
    return this.list;
  }

  getById(id: string) {
    return this.af.database.object(`${this.context.organization}/teachers/${id}`);
  }

  updateTeacherInfo(teacher: Teacher) {
    const key = teacher.$key;

    delete (teacher as any).$key;
    delete (teacher as any).$exists;

    this.af.database.object(`${this.context.organization}/teachers/${key}`).update(teacher);
  }

  deleteTeacher(teacher: Teacher) {
    this.af.database.object(`${this.context.organization}/teachers/${teacher.$key}`).remove();
  }
}
