export class Teacher {
  $key?: string;
  name: string;
  summary?: string;
  age?: number;

  constructor(name: string) {
    this.name = name;

  }
}
