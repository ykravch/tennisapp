import {Component} from '@angular/core';
import {TeacherService} from "../teacher.service";
import {Teacher} from "../teacher";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-teacher-info',
  templateUrl: './teacher-info.component.html',
  styleUrls: ['./teacher-info.component.css'],
  providers: [
    TeacherService
  ]
})
export class TeacherInfoComponent {
  teacher: Teacher;
  editableTeacher: Teacher;
  isEditMode: boolean = false;

  constructor(private teacherService: TeacherService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe((param) => {
      this.refreshTeacherWithId((param as any).id);
    });
  }

  refreshTeacherWithId(id: string) {
    this.teacherService.getById(id).subscribe(teacher => this.teacher = teacher);
  }

  updateSummary(e: KeyboardEvent) {
    this.editableTeacher.summary = (e.target as HTMLTextAreaElement).value;
  }

  editTeacher() {
    this.editableTeacher = (Object as any).assign({}, this.teacher);

    this.isEditMode = true;
  }

  deleteTeacher() {
    if(window.confirm("Are you sure you want to delete teacher?")) {
      this.teacherService.deleteTeacher(this.teacher);
      this.router.navigateByUrl('owner/teachers');
    }
  }

  cancelEdit() {
    this.isEditMode = false;
    this.editableTeacher = null;
  }

  saveTeacher() {
    this.teacherService.updateTeacherInfo(this.editableTeacher);
    this.isEditMode = false;
    this.teacher = (Object as any).assign(this.teacher, this.editableTeacher);
    this.editableTeacher = null;
  }
}
