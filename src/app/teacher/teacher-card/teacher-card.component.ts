import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Teacher} from "../teacher";

@Component({
  selector: 'app-teacher-card',
  templateUrl: './teacher-card.component.html',
  styleUrls: ['./teacher-card.component.css']
})
export class TeacherCardComponent implements OnInit {
  @Input() teacher: Teacher;
  @Output() onClick = new EventEmitter<Teacher>();

  constructor() { }

  ngOnInit() {
  }

  onTeacherClick() {
    this.onClick.emit(this.teacher);
  }
}
