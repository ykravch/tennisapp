import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Teacher} from "../teacher";
import {TeacherService} from "../teacher.service";

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css'],
  providers: [TeacherService]
})
export class AddTeacherComponent {
  model: Teacher;
  submitted: boolean = false;
  active: boolean = true;

  @Output() onAddTeacher = new EventEmitter();

  constructor(private teacherService: TeacherService) {
    this.model = new Teacher("");
  }

  onSubmit() {
    this.teacherService.addTeacher(this.model);
    this.submitted = true;
    this.model = new Teacher("");
    this.onAddTeacher.emit();
    setTimeout(() => this.active = true, 0);
  }

}
