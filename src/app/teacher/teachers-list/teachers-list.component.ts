import { Component } from '@angular/core';
import { FirebaseListObservable} from "angularfire2";
import {Teacher} from "../teacher";
import {TeacherService} from "../teacher.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.css'],
  providers: [TeacherService]
})
export class TeachersListComponent {
  teachers: FirebaseListObservable<Teacher[]>;

  constructor(private teacherService: TeacherService, private router: Router) {
    this.teachers = this.teacherService.getTeachers();
  }

  onTeacherClick(teacher: Teacher) {
    this.router.navigateByUrl(`owner/teachers/${teacher.$key}`);
  }
}
