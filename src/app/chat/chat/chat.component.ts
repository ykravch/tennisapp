import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {ChatService} from "../chat.service";
import {Message} from "../message";
import {FirebaseListObservable} from "angularfire2";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnChanges {
  messages: FirebaseListObservable<Message[]>;

  @Input() groupId: string;

  constructor(private chatService: ChatService) {}

  ngOnChanges() {
    this.messages = this.chatService.getMessages(this.groupId);
  }

  onAddMessage(message) {
    this.chatService.addMessage(this.groupId, message);
  }
}
