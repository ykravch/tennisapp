import {Component, EventEmitter, Output} from '@angular/core';
import {Message} from "../message";
import {ChatService} from "../chat.service";

@Component({
  selector: 'app-chat-add-message',
  templateUrl: './chat-add-message.component.html',
  styleUrls: ['./chat-add-message.component.css'],
  providers: [ChatService]
})
export class ChatAddMessageComponent {

  newMessage: Message;
  submitted: boolean = false;
  active: boolean = true;

  @Output() onAddMessage = new EventEmitter<Message>();

  constructor(private chatService: ChatService) {
    this.newMessage = new Message("");
  }

  onSubmitMessage() {
    this.submitted = true;
    this.onAddMessage.emit(this.newMessage);
    this.newMessage = new Message("");

    setTimeout(() => this.active = true, 0);
  }
}
