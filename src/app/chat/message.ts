export class Message {
  text: string;
  authorId: string;
  name: string;
  postedAt: number;


  constructor(text: string) {
    this.text = text;
  }
}
