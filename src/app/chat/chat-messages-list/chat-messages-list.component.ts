import {Component, OnInit, Input} from '@angular/core';
import {Message} from "../message";
import {FirebaseListObservable} from "angularfire2";

@Component({
  selector: 'app-chat-messages-list',
  templateUrl: './chat-messages-list.component.html',
  styleUrls: ['./chat-messages-list.component.css']
})
export class ChatMessagesListComponent implements OnInit {

  @Input() messages: FirebaseListObservable<Message[]>;

  constructor() { }

  ngOnInit() {
  }

}
