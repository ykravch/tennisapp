import {Injectable} from '@angular/core';
import {Message} from "./message";
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class ChatService {

  constructor(private af: AngularFire, private auth: AuthService) {}

  addMessage(groupId: string, message: Message) {
    console.log("new message:", message);
    const context = this.auth.getContext();

    this.af.database.object(`${context.organization}/users/${this.auth.firebaseUser.uid}`).subscribe(user => {
      this.af.database.list(`${context.organization}/messages/${groupId}`).push({
        text: message.text,
        authorId: user.name,
        name: user.name,
        postedAt: Date.now()
      });
    })
  }

  getMessages(groupId: string): FirebaseListObservable<Message[]> {
    const context = this.auth.getContext();

    return this.af.database.list(`${context.organization}/messages/${groupId}`);
  }
}
