import { Component } from '@angular/core';
import {Group} from "../group";
import {FirebaseListObservable} from "angularfire2";
import {GroupService} from "../group.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css'],
  providers: [GroupService]
})
export class GroupsListComponent {
  groups: FirebaseListObservable<Group[]>;

  constructor(private groupService: GroupService, private router: Router) {
    this.groups = this.groupService.getGroups();
  }

  onGroupClick(group: Group) {
    this.router.navigateByUrl(`owner/groups/${group.$key}`);
  }
}
