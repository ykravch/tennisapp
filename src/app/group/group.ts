export class Group {
  $key?: string;
  title: string;
  teacherName: string;
  teacherId: string;

  constructor(title: string) {
    this.title = title;
  }
}
