import {Component, Output, Input, EventEmitter} from '@angular/core';
import {Group} from "../group";

@Component({
  selector: 'app-group-card',
  templateUrl: './group-card.component.html',
  styleUrls: ['./group-card.component.css']
})
export class GroupCardComponent {

  @Input() group: Group;
  @Output() onClick = new EventEmitter<Group>();

  constructor() { }

  onGroupClick() {
    this.onClick.emit(this.group);
  }
}
