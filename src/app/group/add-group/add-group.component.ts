import {Component, Output, EventEmitter} from '@angular/core';
import {Group} from "../group";
import {GroupService} from "../group.service";

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css'],
  providers: [GroupService]
})
export class AddGroupComponent {

  model: Group;
  submitted: boolean = false;
  active: boolean = true;

  @Output() onAddGroup = new EventEmitter();

  constructor(private groupService: GroupService) {
    this.model = new Group("");
  }

  onSubmit() {
    this.groupService.addGroup(this.model);
    this.submitted = true;
    this.model = new Group("");
    this.onAddGroup.emit();
    setTimeout(() => this.active = true, 0);
  }
}
