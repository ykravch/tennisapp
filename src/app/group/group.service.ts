import { Injectable } from '@angular/core';
import {Group} from "./group";
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class GroupService {
  context: any; //TODO: fix any
  list: FirebaseListObservable<Group[]>;

  constructor(private af: AngularFire, private auth: AuthService) {
    this.context = this.auth.getContext();
    this.list = this.af.database.list(`${this.context.organization}/groups`);
  }

  addGroup(group: Group) {
    this.list.push(group);
  }

  getGroups(): FirebaseListObservable<Group[]> {
    return this.list;
  }

  getById(id: string) {
    return this.af.database.object(`${this.context.organization}/groups/${id}`);
  }

  updateGroupInfo(group: Group) {
    const key = group.$key;

    delete (group as any).$key;
    delete (group as any).$exists;

    this.af.database.object(`${this.context.organization}groups/${key}`).update(group);
  }

  deleteGroup(group: Group) {
    this.af.database.object(`${this.context.organization}groups/${group.$key}`).remove();
  }
}
