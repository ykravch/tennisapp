import { Component } from '@angular/core';

@Component({
  selector: 'app-groups-page',
  templateUrl: './groups-page.component.html',
  styleUrls: ['./groups-page.component.css']
})
export class GroupsPageComponent {

  shouldRenderAddForm: boolean = false;

  constructor() { }

  renderForm(shouldRenderForm) {
    this.shouldRenderAddForm = shouldRenderForm;
  }
}
