import { Component, OnInit } from '@angular/core';
import {Group} from "../group";
import {GroupService} from "../group.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-group-info',
  templateUrl: './group-info.component.html',
  styleUrls: ['./group-info.component.css'],
  providers: [GroupService]
})
export class GroupInfoComponent {

  group: Group;
  editableGroup: Group;
  isEditMode: boolean = false;

  constructor(private groupService: GroupService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe((param) => {
      this.refreshGroupWithId((param as any).id);
    });
  }

  refreshGroupWithId(id: string) {
    this.groupService.getById(id).subscribe(group => this.group = group);
  }

  editGroup() {
    this.editableGroup = (Object as any).assign({}, this.group);

    this.isEditMode = true;
  }

  deleteGroup() {
    if(window.confirm("Are you sure you want to delete group?")) {
      this.groupService.deleteGroup(this.group);
      this.router.navigateByUrl('owner/groups');
    }
  }

  cancelEdit() {
    this.isEditMode = false;
    this.editableGroup = null;
  }

  saveGroup() {
    this.groupService.updateGroupInfo(this.editableGroup);
    this.isEditMode = false;
    this.group = (Object as any).assign(this.group, this.editableGroup);
    this.editableGroup = null;
  }

}
