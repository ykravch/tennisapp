import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerPageComponent } from "./owner-page/owner-page.component";
import { TeachersPageComponent } from "./teacher/teachers-page/teachers-page.component";
import {OwnerDashboardComponent} from "./owner-dashboard/owner-dashboard.component";
import {GroupsPageComponent} from "./group/groups-page/groups-page.component";
import {StudentsPageComponent} from "./students/students-page/students-page.component";
import {TeacherInfoComponent} from "./teacher/teacher-info/teacher-info.component";
import {EmptyComponent} from "./empty/empty.component";
import {GroupInfoComponent} from "./group/group-info/group-info.component";
import {IsUserLogginedService} from "./auth/is-user-loggined.service";
import {Permissions} from "./auth/permissions";
import {StandardUser} from "./auth/standard-user";
import {LoginComponent} from "./auth/login/login.component";
import {AuthService} from "./auth/auth.service";
import {LogoutComponent} from "./auth/logout/logout.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  // {path: 'register', component: RegisterUserComponent},
  {path: 'logout',  component: LogoutComponent},

  {path: 'owner', component: OwnerPageComponent, canActivate: [IsUserLogginedService], children: [
    {path: 'dashboard', component: OwnerDashboardComponent},
    {path: 'teachers', component: TeachersPageComponent, children: [
      {path: ':id', component: TeacherInfoComponent},
      {path: '', component: EmptyComponent}
    ]},
    {path: 'groups', component: GroupsPageComponent, children: [
      {path: ':id', component: GroupInfoComponent},
      {path: '', component: EmptyComponent}
    ]},
    {path: 'students', component: StudentsPageComponent},
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
  ]},

  {path: 'teacher', component: EmptyComponent},

  {path: '', redirectTo: 'owner/dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [IsUserLogginedService, Permissions, StandardUser, AuthService]
})
export class TennisAppRoutingModule { }
