import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Permissions} from "./permissions";
import {StandardUser} from "./standard-user";
import {AuthService} from "./auth.service";

@Injectable()
export class IsUserLogginedService implements CanActivate {

  constructor(private permissions: Permissions, private auth: AuthService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {

    return this.permissions.isLoggedIn(this.auth.isLoggedIn());
  }

}
