import {StandardUser} from "./standard-user";
export class Permissions {

  isLoggedIn(user: StandardUser): boolean {
    return !!user;
  }

  canOpenOwnerPage(user: StandardUser): boolean {
    return user.role === 'owner';
  }
}
