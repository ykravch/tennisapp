/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { IsUserLogginedService } from './is-user-loggined.service';

describe('Service: IsUserLoggined', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsUserLogginedService]
    });
  });

  it('should ...', inject([IsUserLogginedService], (service: IsUserLogginedService) => {
    expect(service).toBeTruthy();
  }));
});
