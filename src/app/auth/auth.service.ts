import { Injectable } from '@angular/core';
import {StandardUserCredentials} from "./standard-user-credentials";
import {AngularFire, AuthProviders, FirebaseAuthState, FirebaseObjectObservable} from "angularfire2";
import {StandardUser} from "./standard-user";
import {Router} from "@angular/router";
import {Context} from "./context";

declare var firebase:any;

@Injectable()
export class AuthService {
  user: StandardUser;
  firebaseUser: FirebaseAuthState;
  loginP: any;
  context: Context;
  userSubscriber: any;

  constructor(private af: AngularFire, private router: Router) {
    this.userSubscriber = this.af.auth.subscribe(function(user) {
      if (!user) { // if we logged out remotely
        this.router.navigateByUrl(`logout`);
        this.user = null;
        this.firebaseUser = null;

        console.warn('logged out');
        return;
      }

      if (!this.user) {
        this.user = JSON.parse(window.localStorage.getItem('user'));
        this.context = JSON.parse(window.localStorage.getItem('context'));
      }

      this.firebaseUser = user;
      this.refreshUser();

      console.warn("User changed:", user);
    }.bind(this));
  }

  isLoggedIn() {
    return this.userSubscriber;
  }

  getCurrentUser(): StandardUser {
    return this.user;
  }

  getContext() {
    return this.context;
  }

  resetPassword(email) {
    // auth.sendPasswordResetEmail(email).then(() => console.log("Success"), error => console.log(error));
    firebase.auth().sendPasswordResetEmail(email).then(() => console.log("Reset Success"), error => console.log(error));
  }

  // registerUser(userCredentials: StandardUserCredentials) {
  //   this.af.auth.createUser(userCredentials).then(newUser => {
  //     // Save user to custom user table
  //     console.log("new user:" , newUser);
  //
  //     this.af.database.object(`users/${newUser.uid}`).set({
  //       role: "owner"
  //     });
  //
  //
  //
  //   }, error => console.log(error));
  // } window.setTimeout(() => this.router.navigateByUrl(`/owner/dashboard`), 1000);

  loginUser(userCredentials: StandardUserCredentials) {
    this.loginP = this.af.auth.login(userCredentials);

    this.loginP.then(successRes => {
      const userId = successRes.uid;
      this.af.database.object(`organizationByUser/${userId}`).subscribe(orgId => {
        this.af.database.object(`${orgId.$value}/users/${userId}`).subscribe(user => {
          this.user = user;
          this.context = {
            organization: orgId.$value as string
          };

          window.localStorage.setItem('user', JSON.stringify(user));
          window.localStorage.setItem('context', JSON.stringify(this.context));

          this.router.navigateByUrl(`/${user.role}`);
        })
      });
    }, error => {
      console.error("Authentication error:", error);
    })
  }

  logout() {
    window.localStorage.removeItem('user');
    window.localStorage.removeItem('context');

    this.af.auth.logout();
  }

  private refreshUser() {
    const userId = this.firebaseUser.uid;

    this.af.database.object(`organizationByUser/${userId}`).subscribe(orgId => {
      this.af.database.object(`${orgId.$value}/users/${userId}`).subscribe(user => {
        this.user = user;
        this.context = {
          organization: orgId.$value as string
        };
      });
    });

  }
}
