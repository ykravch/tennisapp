import { Component } from '@angular/core';
import {StandardUserCredentials} from "../standard-user-credentials";
import {AuthService} from "../auth.service";
import {ActivatedRoute, Router} from "@angular/router";
// import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  // orgId: string;
  model: StandardUserCredentials;

  constructor(private auth: AuthService, route: ActivatedRoute) {
    this.model = new StandardUserCredentials("", "");
    console.log('login');

    if ((route.data as any).value.logout) {
      this.auth.logout();
    }

    // this.route.params.subscribe((param) => {
    //   this.orgId = (param as any).orgId;
    // });
  }

  onLogin() {
    this.auth.loginUser(this.model);
  }

}
