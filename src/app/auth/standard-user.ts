export class StandardUser {
  email?: string;
  username?: string;
  name?: string;
  organization?: string;
  role: "owner" | "teacher" | "student";
}
